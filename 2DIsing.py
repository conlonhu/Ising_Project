#!/usr/bin/env python
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import random
import IsingModules as Is


#constants
N = 100 #dimensions of matrix
J = 1 #energy exchange coefficient, swith to -1 to observe antiferromagnetism below Tc
h = 0 #magnetic field, must be set to 0 to compute observables 
counts = 100
T = 1 #temperature, keep below ~2.26 for ferromagnetism, above 2.26 for paramagnetism
k=1 #boltzmann constant

class Ising_lattice: 
	def __init__(self,N):	#create initial matrix of size NxN
		self.N=N
		self.matrix_lattice()
	
	def matrix_lattice(self):
		self.lattice = np.zeros((N,N), dtype=int) #creates initial matrix of zeroes
		for x in range(N):	
			for y in range(N):
				self.lattice[x,y]=random.choice([1,-1]) #randomly chooses values of 1 and -1 and appends matrix
    
                
lattice1=Ising_lattice(N) #makes randomized matrix an object

print Is.metropolis(lattice1.lattice, counts,N, T, J, h, k)
#plt.imshow(Is.metropolis(lattice1.lattice, counts,N, T, J, h, k),cmap='bwr',interpolation="none") 
#plt.show() #plots Ising model in equilibrium 


Trange=np.arange(0.1,4.1,0.1) #array of temperates ranging from subcritical to supercritical

for T in Trange: #overwrites single T allowing Ising model for range to be computed
	print 'Tempurature:',T

	magmatrix = Is.metropolis(lattice1.lattice, counts,N, T, J, h, k) #creates matrix put through the metropolis algorithim 
	m = Is.magnetisation(magmatrix,N) #creates new matrix of average magnetisation
	print 'Average Magnetisation:',m
	plt.figure(1)
	plt.title('Average Magnetisation per spin vs Temperature')
	plt.xlabel('Temperature (J/k$_B$)')	
	plt.ylabel('Average Magnetisation per spin(J)')
	plt.plot(T, m, 'bo')

	matrixformag_sq = Is.metropolis(lattice1.lattice, counts,N, T, J, h, k)#creates matrix put through the metropolis algorithim 
	m_sq = Is.magnetisation_sq(matrixformag_sq,N) #creates new matrix of average squared magnetisation

	Xv = (m_sq-m**2)/(k*T) #calculates magnetic susceptibility 
	print 'Magnetic Susceptibility:',Xv
	plt.figure(2)
	plt.title('Magnetic Susceptibility vs Temperature')
	plt.xlabel('Temperature (J/k$_B$)')	
	plt.ylabel('Magnetic Susceptibility')
	plt.plot(T, Xv, 'bo')

	matrixforenergy = Is.metropolis(lattice1.lattice, counts,N, T, J, h, k)#creates matrix put through the metropolis algorithim 
	ener = Is.energy(matrixforenergy,N,J, h)#creates new matrix of average energy
	print 'Average Energy:',ener
	plt.figure(3)
	plt.title('Average Energy per spin vs Temperature')
	plt.xlabel('Temperature (J/k$_B$)')
	plt.ylabel('Average Energy per spin(J)')
	plt.plot(T, ener, 'bo')


	matrixforenergy_sq = Is.metropolis(lattice1.lattice, counts,N, T, J, h, k)#creates matrix put through the metropolis algorithim 
	ener_sq = Is.energy_sq(matrixforenergy_sq,N,J, h) #creates new matrix of average squared energy

	Cv = (ener_sq - (ener**2))/(k*T**2) #calculates specific heat
	print 'Specific Heat:',Cv
	print ','
	plt.figure(4)
	plt.title('Specific Heat Capacity vs Temperature')
	plt.xlabel('Temperature (J/k$_B$)')	
	plt.ylabel('Specific Heat Capacity')
	plt.plot(T, Cv, 'bo')
plt.show()





