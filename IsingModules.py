#!/usr/bin/env python
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import random

#function that sums up all neighbouring sites of the inital position. %N imposes a boundary condition so the function knows when to stop 
def neighbours(matrix,x,y,N): 
	s1 = matrix[x, (y+1) %N]
	s2 = matrix[x, (y-1) %N]
	s3 = matrix[(x+1) %N, y]
	s4 = matrix[(x-1) %N, y]
	return s1 + s2 + s3 + s4

#function for change in energy
def deltaE(matrix, x, y, N, J, h): 
	return -(2*J*matrix[x,y]*(neighbours(matrix,x,y,N)))+2*h*matrix[x,y] 

#metropolis algorithim 
def metropolis(matrix, counts,N, T, J, h, k):
	for n in range (counts):
		for y in range(0, N):
		    for x in range(0,N):
		    	if deltaE(matrix, x, y, N, J, h)>=0: 
		    		matrix[x,y] *= -1 #if energy change is greater than/equal to 0, flips spin
		    	else:
		    		r = random.random() #generates random number
		    		if r<np.exp(deltaE(matrix, x, y, N, J, h)/(k*T)):		
		    				matrix[x,y] *= -1 #if random number generated between 0 and 1 is less than exp^dE/k*T flips spin
	return matrix


def magnetisation(matrix,N):
	return (np.abs(np.sum(matrix)))/float(N**2.0) #sums up all values in the matrix(1 or -1), takes the absolute value and averages it

def magnetisation_sq(matrix,N):
	return (np.sum(matrix**2))/float(N**2.0) #matrix**2 squares all values in the matrix, then sums up all values in the matrix(1 or -1), takes the absolute value and averages it. Will always return 1

def H(matrix, x,y, N, J, h): #Computes value of the Hamiltonian for Ising Model
    return -((J*matrix[x,y]*(neighbours(matrix,x,y,N)))+h*matrix[x,y])

def energy(matrix,N, J, h): #returns average energy per spin 
    e=0 #initialise energy
    for y in range(0, N):
        for x in range(0,N):
            e=e+0.5*H(matrix, x,y, N, J, h) #increments energy by 1/2 Hamiltonian
    return e/N**2 #averages energy per spin


def H_sq(matrix, x,y,N,  J, h): #computes and squares value of the Hamiltonian for Ising Model
    return (-((J*matrix[x,y]*(neighbours(matrix,x,y,N)))+h*matrix[x,y]))**2

def energy_sq(matrix,N, J, h): #returns average squared energy per spin
    e_sq=0
    for y in range(0, N):
        for x in range(0,N):
            e_sq=e_sq+0.25*H_sq(matrix, x,y, N, J, h) #increments energy by 1/4 Hamiltonian
    return e_sq/N**2 #averages energy per spin



	
