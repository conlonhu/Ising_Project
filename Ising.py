#!/usr/bin/env python
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import random
import IsingModules1D as Is1D
import IsingModules2D as Is2D
import IsingModules3D as Is3D
import IsingPlots as Isplt

#constants
N = 100											#dimensions of matrix
J = 1 											#energy exchange coefficient, swith to -1 to observe antiferromagnetism below Tc, unit Joules
h = 0 											#magnetic field, must be set to 0 to compute observables 
T = 0.5											#temperature, keep below ~Tc for ferromagnetism, above Tc for paramagnetism, unit J/kb
k=1 											#boltzmann constant, J and kb set to one to return temperature in J/kb
counts = 500

class initial_lattice: 
	def __init__(self,N):								#create initial matrix of size NxN
		self.N=N
		self.matrix_lattice()
		self.array_1D()
		self.matrix_3D()

	def array_1D(self):
		self.line = np.random.choice([-1, 1], (N))

	def matrix_lattice(self):
            	self.lattice = np.random.choice([-1, 1], (N,N))

	def matrix_3D(self):
		self.cube = np.random.choice([-1, 1], (N,N,N)) 

    
##########################################
"""2D Ising Model"""
##########################################
"""
lattice1=initial_lattice(N) 								#makes randomized matrix an object
plt.imshow(lattice1.lattice,cmap='bwr',interpolation="none")
plt.title('Initial Ising Model' ) 
plt.colorbar(ticks=range(-1,2), label= 'Spin')
plt.clim(-1,1)
plt.show() 										#plots initial lattice


mat = Is2D.metropolis(lattice1.lattice, counts,N, T, J, h, k)
print mat
plt.imshow(mat,cmap='bwr',interpolation="none")
plt.title('Ising Model after %i sweeps' %counts ) 
plt.colorbar(ticks=range(-1,2), label= 'Spin')
plt.clim(-1,1)
plt.show() 										#plots Ising model in equilibrium


Trange=np.arange(0.1,4.1,0.1) 								#array of temperates ranging from subcritical to supercritical
M_list=[]
X_list=[]
E_list=[]
Cv_list=[]

for T in Trange: 									#overwrites single T allowing Ising model for range to be computed
	print 'Temperature:',T

	up_matrix = Is2D.metropolis(lattice1.lattice, counts,N, T, J, h, k) 		#creates matrix put through the metropolis algorithim 

	M = Is2D.magnetisation(up_matrix,N) 						#creates new matrix of average magnetisation
	print 'Average Magnetisation:',M
	M_list.append(M)

 
	M_sq = Is2D.magnetisation_sq(up_matrix,N) 					#creates new matrix of average squared magnetisation
	X = (M_sq-M**2)/(k*T) 								#calculates magnetic susceptibility 
	print 'Magnetic Susceptibility:',X	
	X_list.append(X)
	

	E = Is2D.energy(up_matrix,N,J, h)						#creates new matrix of average energy
	print 'Average Energy:',E
	E_list.append(E)


	E_sq = Is2D.energy_sq(up_matrix,N,J, h) 					#creates new matrix of average squared energy
	Cv = (E_sq - (E**2))/(k*T**2) 							#calculates specific heat
	print 'Specific Heat:',Cv
	Cv_list.append(Cv)
	
	print ','

plt.figure(1), plt.show(Isplt.magplot(Trange,M_list))
plt.figure(2), plt.show(Isplt.magsusplot(Trange, X_list))
plt.figure(3), plt.show(Isplt.energyplot(Trange, E_list))
plt.figure(4), plt.show(Isplt.specheatplot(Trange, Cv_list))

print 'Phase Transition at:', Trange[np.argmax(X_list)] 				#return Tc
print 'Phase Transition at:', Trange[np.argmax(Cv_list)] 				#return Tc
"""

##########################################
"""2D Triangular Lattice Ising Model"""
##########################################
"""
tri_lattice=initial_lattice(N) 								#makes randomized matrix an object

Trange=np.arange(0.1,6.1,0.1) 								#array of temperates ranging from subcritical to supercritical
M_list=[]
X_list=[]
E_list=[]
Cv_list=[]

for T in Trange: 									#overwrites single T allowing Ising model for range to be computed
	print 'Temperature:',T

	up_matrix = Is2D.tri_metropolis(tri_lattice.lattice, counts,N, T, J, h, k) 	#creates matrix put through the metropolis algorithim 

	M = Is2D.magnetisation(up_matrix,N) 						#creates new matrix of average magnetisation
	print 'Average Magnetisation:',M
	M_list.append(M)


	M_sq = Is2D.magnetisation_sq(up_matrix,N) 					#creates new matrix of average squared magnetisation

	X = (M_sq-M**2)/(k*T)								#calculates magnetic susceptibility 
	print 'Magnetic Susceptibility:',X
	X_list.append(X)


	E = Is2D.tri_energy(up_matrix,N,J, h)						#creates new matrix of average energy
	print 'Average Energy:',E
	E_list.append(E)


	E_sq = Is2D.tri_energy_sq(up_matrix,N,J, h) 					#creates new matrix of average squared energy
	Cv = (E_sq - (E**2))/(k*T**2) 							#calculates specific heat
	print 'Specific Heat:',Cv
	Cv_list.append(Cv)

	print ','

plt.figure(1), plt.show(Isplt.magplot(Trange,M_list))
plt.figure(2), plt.show(Isplt.magsusplot(Trange, X_list))
plt.figure(3), plt.show(Isplt.energyplot(Trange, E_list))
plt.figure(4), plt.show(Isplt.specheatplot(Trange, Cv_list))

print 'Phase Transition at:', Trange[np.argmax(X_list)]					#return Tc
print 'Phase Transition at:', Trange[np.argmax(Cv_list)]				#return Tc

"""
##########################################
"""1D Ising Model"""
##########################################
"""
line1 = initial_lattice(N)
print Is1D.metropolis(line1.line, counts,N, T, J, h, k)

Trange=np.arange(0.1,4.1,0.1) 								#array of temperates ranging from subcritical to supercritical
M_list=[]
X_list=[]
E_list=[]
Cv_list=[]

for T in Trange: 									#overwrites single T allowing Ising model for range to be computed
	print 'Temperature:',T

	up_matrix = Is1D.metropolis(line1.line, counts,N, T, J, h, k) 			#creates matrix put through the metropolis algorithim 

	M = Is1D.magnetisation(up_matrix,N) 						#creates new matrix of average magnetisation
	print 'Average Magnetisation:',M
	M_list.append(M)

	
	M_sq = Is1D.magnetisation_sq(up_matrix,N) 					#creates new matrix of average squared magnetisation
	X = (M_sq-M**2)/(k*T) 								#calculates magnetic susceptibility 
	print 'Magnetic Susceptibility:',X
	X_list.append(X)


	E = Is1D.energy(up_matrix,N,J, h)						#creates new matrix of average energy
	print 'Average Energy:',E
	E_list.append(E)


	E_sq = Is1D.energy_sq(up_matrix,N,J, h) 					#creates new matrix of average squared energy
	Cv = (E_sq - (E**2))/(k*T**2) 							#calculates specific heat
	print 'Specific Heat:',Cv
	Cv_list.append(Cv)

	print ','

plt.figure(1), plt.show(Isplt.magplot(Trange,M_list))
plt.figure(2), plt.show(Isplt.magsusplot(Trange, X_list))
plt.figure(3), plt.show(Isplt.energyplot(Trange, E_list))
plt.figure(4), plt.show(Isplt.specheatplot(Trange, Cv_list))
"""
##########################################
"""3D Ising Model"""
##########################################
"""
N = 20
counts = 100										#resetting dimension and counts to reduce computation time
cube1 = initial_lattice(N)
#print Is3D.metropolis(cube1.cube, counts,N, T, J, h, k)

Trange=np.arange(0.1,6.1,0.1) 								#array of temperates ranging from subcritical to supercritical
M_list=[]
X_list=[]
E_list=[]
Cv_list=[]

for T in Trange: 									#overwrites single T allowing Ising model for range to be computed
	print 'Temperature:',T

	up_matrix = Is3D.metropolis(cube1.cube, counts,N, T, J, h, k) 			#creates matrix put through the metropolis algorithim 

	M = Is3D.magnetisation(up_matrix,N) 						#creates new matrix of average magnetisation
	print 'Average Magnetisation:',M
	M_list.append(M)


	M_sq = Is3D.magnetisation_sq(up_matrix,N) 					#creates new matrix of average squared magnetisation
	X = (M_sq-M**2)/(k*T) 								#calculates magnetic susceptibility 
	print 'Magnetic Susceptibility:',X
	X_list.append(X)

	 
	E = Is3D.energy(up_matrix,N,J, h)						#creates new matrix of average energy
	print 'Average Energy:',E
	E_list.append(E)

 
	E_sq = Is3D.energy_sq(up_matrix,N,J, h) 					#creates new matrix of average squared energy
	Cv = (E_sq - (E**2))/(k*T**2) 							#calculates specific heat
	print 'Specific Heat:',Cv
	Cv_list.append(Cv)

	print ','

plt.figure(1), plt.show(Isplt.magplot(Trange,M_list))
plt.figure(2), plt.show(Isplt.magsusplot(Trange, X_list))
plt.figure(3), plt.show(Isplt.energyplot(Trange, E_list))
plt.figure(4), plt.show(Isplt.specheatplot(Trange, Cv_list))

print 'Phase Transition at:', Trange[np.argmax(X_list)]					#return Tc
print 'Phase Transition at:', Trange[np.argmax(Cv_list)]				#return Tc
"""






