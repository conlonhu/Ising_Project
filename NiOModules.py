#!/usr/bin/env python
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import random

#function that sums up all neighbouring sites of the inital position. %N imposes a boundary condition 
def diagonal_neighbours(matrix,x,y,N): 					#diagonal neighbours
        d1 = matrix[(x+1)%N, (y+1) %N]
        d2 = matrix[(x+1)%N, (y-1) %N]
        d3 = matrix[(x-1)%N, (y+1)%N]
        d4 = matrix[(x-1) %N, (y-1)%N]
        return d1 + d2 + d3 + d4

def lateral_neighbours(matrix,x,y,N): 					#neighbours along axes
        l1 = matrix[x, (y+2) %N]
        l2 = matrix[x, (y-2) %N]
        l3 = matrix[(x+2) %N, y]
        l4 = matrix[(x-2) %N, y]
        return l1 + l2 + l3 + l4

#function for change in energy. Includes additional J2 term
def deltaE(matrix, x, y, N, J1, J2, h): 
        return -(2*J1*matrix[x,y]*(diagonal_neighbours(matrix,x,y,N)))-(2*J2*matrix[x,y]*(lateral_neighbours(matrix,x,y,N)))+2*h*matrix[x,y] 

#metropolis algorithim 
def metropolis(matrix, counts,N, T, J1,J2, h, k):
	for p in range (counts):
            for y in range(0, N):
                for x in range(0,N):					#iterates [x,y] sequentially 
		    dE = deltaE(matrix, x, y, N, J1, J2, h)
                    if dE>=0: 
                        matrix[x,y] *= -1 				#if energy change is greater than/equal to 0, flips spin
                    else:
                        r = random.random() 				#generates random number
                        if r<np.exp(dE/(k*T)):      
                                matrix[x,y] *= -1 			#if random number generated between 0 and 1 is less than exp^dE/k*T flips spin
        return matrix




def H(matrix, x,y, N, J1, J2, h): 					#Computes value of the Hamiltonian for Ising Model
    return -(J1*matrix[x,y]*(diagonal_neighbours(matrix,x,y,N)))-(J2*matrix[x,y]*(lateral_neighbours(matrix,x,y,N)))+h*matrix[x,y]


def H_sq(matrix, x,y,N,  J1, J2, h): 					#computes and squares value of the Hamiltonian for Ising Model
    return (-(J1*matrix[x,y]*(diagonal_neighbours(matrix,x,y,N)))-(J2*matrix[x,y]*(lateral_neighbours(matrix,x,y,N)))+h*matrix[x,y])**2



"""Magnetisation Modules"""

def magnetisation(matrix,N):
	return (np.abs(np.sum(matrix)))/float(N**2.0) 			#sums up all values in the matrix(1 or -1), takes the absolute value and averages it

def magnetisation_sq(matrix,N):
	return (np.sum(matrix**2))/float(N**2.0) #			matrix**2 squares all values in the matrix, then sums up all values in the matrix(1 or -1), takes the absolute value and averages it. 


"""Energy Modules"""

def energy(matrix,N, J1, J2, h): 					#returns average energy per spin 
    energy=0 								#initialise energy
    for y in range(0, N):
        for x in range(0,N):
	    Ham = H(matrix, x,y, N, J1, J2, h) 
            energy=energy+Ham 						#increments energy by Hamiltonian. Removed 1/2 term because no longer double counting pairs 
    return energy/N**2 							#averages energy per spin


def energy_sq(matrix,N, J1, J2, h): 					#returns average squared energy per spin
    energy_sq=0
    for y in range(0, N):
        for x in range(0,N):
	    Ham_sq = H_sq(matrix, x,y, N, J1, J2, h)
            energy_sq=energy_sq+Ham_sq					#increments energy by Hamiltonian_sq. Removed 1/4 term because no longer double counting pairs
    return energy_sq/N**2 						#averages energy per spin


