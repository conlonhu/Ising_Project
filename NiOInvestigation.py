#!/usr/bin/env python
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import random
import NiOModules as NiO
import IsingPlots as Isplt

#constants
N = 100 										#dimensions of matrix
J1 = 2.3 										#diagonal energy exchange coefficient, unit meV
J2 = -21										#lateral energy exchange coefficient, unit meV
h = 0 											#magnetic field, must be set to 0 to compute observables 
T = 10 											#temperature, unit meV/kb
k=1 											#boltzmann constant, set to 1 to return temp in units meV/kB
counts = 500

class initial_lattice: 
        def __init__(self,N):   							#create initial matrix of size NxN
            self.N=N
            self.matrix_lattice()

        def matrix_lattice(self):
            self.lattice = np.random.choice([-1, 1], (N, N))
            self.lattice[::2, ::2] = 0							#sets zero as odd index value for every odd array (1,3,5,...)
            self.lattice[1::2, 1::2] = 0						#sets zero as odd index value for every odd array (0,2,4,...)	

lattice1=initial_lattice(N) 
plt.imshow(lattice1.lattice,cmap='bwr',interpolation="none") 
plt.title('Initial Ising Model of NiO' ) 
plt.colorbar(ticks=range(-1,2), label= 'Spin')
plt.clim(-1,1)
plt.show()										#plots initial lattice

print lattice1.lattice


NiOmat = NiO.metropolis(lattice1.lattice, counts,N, T, J1, J2, h, k)
print NiOmat
plt.imshow(NiOmat,cmap='bwr',interpolation="none") 
plt.imshow(NiOmat,cmap='bwr',interpolation="none")
plt.title('Ising Model of NiO in equilibrium' ) 
plt.colorbar(ticks=range(-1,2), label= 'Spin')
plt.clim(-1,1)
plt.show() 										#plots Ising model in equilibrium 


Trange=np.arange(1,41,1) 								#array of temperates ranging from subcritical to supercritical

M_list=[]
X_list=[]
E_list=[]
Cv_list=[]

for T in Trange: 									#overwrites single T allowing Ising model for range to be computed
	print 'Temperature:',T

	up_matrix = NiO.metropolis(lattice1.lattice, counts,N, T, J1, J2, h, k) 	#creates matrix put through the metropolis algorithim 

	M = NiO.magnetisation(up_matrix,N) 						#creates new matrix of average magnetisation
	print 'Average Magnetisation:',M
	M_list.append(M)

	
	E = NiO.energy(up_matrix,N, J1, J2, h)						#creates new matrix of average energy
	print 'Average Energy:',E
	E_list.append(E)

	print ','

plt.figure(1), plt.show(Isplt.NiOmagplot(Trange,M_list))
plt.figure(2), plt.show(Isplt.NiOmagplot2(Trange,M_list))
plt.figure(3), plt.show(Isplt.NiOenergyplot(Trange, E_list))





