#!/usr/bin/env python
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import random
import IsingModules1D as Is1D
import IsingModules2D as Is2D
import IsingModules3D as Is3D

def magplot(T,x):
	plt.title('Average Magnetisation per spin vs Temperature')
	plt.xlabel('Temperature (J/k$_B$)')	
	plt.ylabel('Average Magnetisation per spin($\mu$)')
	return plt.plot(T, x, 'bo')

def magsusplot(T,x):
	plt.title('Magnetic Susceptibility vs Temperature')
	plt.xlabel('Temperature (J/k$_B$)')	
	plt.ylabel('Magnetic Susceptibility($\mu$/k$_B$)')
	return plt.plot(T, x, 'bo')

def energyplot(T,x):
	plt.title('Average Energy per spin vs Temperature')
	plt.xlabel('Temperature (J/k$_B$)')
	plt.ylabel('Average Energy per spin(J)')
	return plt.plot(T, x, 'bo')
	
def specheatplot(T,x):
	plt.title('Specific Heat Capacity vs Temperature')
	plt.xlabel('Temperature (J/k$_B$)')	
	plt.ylabel('Specific Heat Capacity(J/k$_B$$^2$)')
	return plt.plot(T, x, 'bo')

def NiOmagplot(T,x):
	plt.title('Average Magnetisation per spin vs Temperature')
	plt.xlabel('Temperature (meV/k$_B$)')	
	plt.ylabel('Average Magnetisation per spin($\mu$)')
	return plt.plot(T, x, 'bo')

def NiOmagplot2(T,x):
	plt.title('Average Magnetisation per spin vs Temperature')
	plt.xlabel('Temperature (meV/k$_B$)')	
	plt.ylabel('Average Magnetisation per spin($\mu$)')
	plt.ylim( -0.5, 0.5 ) 
	return plt.plot(T, x, 'bo')

def NiOenergyplot(T,x):
	plt.title('Average Energy per spin vs Temperature')
	plt.xlabel('Temperature (meV/k$_B$)')
	plt.ylabel('Average Energy per spin(meV)')
	return plt.plot(T, x, 'bo')



