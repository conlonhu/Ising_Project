#!/usr/bin/env python
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import random


##########################################
"""2D Ising Model Modules"""
##########################################

#function that sums up all neighbouring sites of the inital position. %N imposes a boundary condition 
def neighbours(matrix,x,y,N): 
	s1 = matrix[x, (y+1) %N]
	s2 = matrix[x, (y-1) %N]
	s3 = matrix[(x+1) %N, y]
	s4 = matrix[(x-1) %N, y]
	return s1 + s2 + s3 + s4

#function for change in energy
def deltaE(matrix, x, y, N, J, h): 
	return -(2*J*matrix[x,y]*(neighbours(matrix,x,y,N)))+2*h*matrix[x,y] 

#metropolis algorithim 
def metropolis(matrix, counts,N, T, J, h, k):
	for p in range (counts):
		for y in range(0, N):							#iterates [x,y] sequentially 
		    for x in range(0,N):
			dE = deltaE(matrix, x, y, N, J, h)
		    	if dE>=0: 
		    		matrix[x,y] *= -1 					#if energy change is greater than/equal to 0, flips spin
		    	else:
		    		r = random.random() 					#generates random number
		    		if r<np.exp(dE/(k*T)):		
		    				matrix[x,y] *= -1 			#if random number generated between 0 and 1 is less than exp^dE/k*T flips spin
	return matrix

def H(matrix, x,y, N, J, h): 								#Computes value of the Hamiltonian for Ising Model
    return -((J*matrix[x,y]*(neighbours(matrix,x,y,N)))+h*matrix[x,y])


def H_sq(matrix, x,y,N,  J, h): 							#computes and squares value of the Hamiltonian for Ising Model
    return (-((J*matrix[x,y]*(neighbours(matrix,x,y,N)))+h*matrix[x,y]))**2



##########################################
"""2D Energy Functions"""
##########################################
def energy(matrix,N, J, h): 								#returns average energy per spin 
    energy=0 										#initialise energy 	
    for y in range(0, N):
        for x in range(0,N):
            Ham = H(matrix, x,y, N, J, h)
            energy=energy+0.5*Ham 							#increments energy by 1/2 Hamiltonian
    return energy/N**2 									#averages energy per spin


def energy_sq(matrix,N, J, h): 								#returns average squared energy per spin
    energy_sq=0	
    for y in range(0, N):
        for x in range(0,N):
            Ham_sq = H_sq(matrix, x,y, N, J, h) 
            energy_sq=energy_sq+0.25*Ham_sq						#increments energy by 1/4 Hamiltonian_sq
    return energy_sq/N**2 								#averages energy per spin


##########################################
"""Triangular Lattice Modules"""
##########################################

#function that sums up all neighbouring sites of the inital position. %N imposes a boundary condition  
def tri_neighbours(matrix,x,y,N): 
	t1 = matrix[x, (y+1) %N]
	t2 = matrix[x, (y-1) %N]
	t3 = matrix[(x+1) %N, y]
	t4 = matrix[(x-1) %N, y]
	if x%2==0:
		t5 = matrix[(x+1)%N, (y+1) %N]
		t6 = matrix[(x+1)%N, (y-1) %N]
	else: 
		t5 = matrix[(x-1)%N, (y-1) %N]
		t6 = matrix[(x-1)%N, (y+1) %N]
	return t1 + t2 + t3 + t4 + t5 + t6

#function for change in energy
def tri_deltaE(matrix, x, y, N, J, h): 
	return -(2*J*matrix[x,y]*(tri_neighbours(matrix,x,y,N)))+2*h*matrix[x,y] 

#metropolis algorithim 
def tri_metropolis(matrix, counts,N, T, J, h, k):
	for p in range (counts):
		for y in range(0, N):
		    for x in range(0,N):
			dE = tri_deltaE(matrix, x, y, N, J, h)
		    	if dE>=0: 
		    		matrix[x,y] *= -1 					#if energy change is greater than/equal to 0, flips spin
		    	else:
		    		r = random.random() 					#generates random number
		    		if r<np.exp(dE/(k*T)):		
		    				matrix[x,y] *= -1 			#if random number generated between 0 and 1 is less than exp^dE/k*T flips spin
	return matrix

def tri_H(matrix, x,y, N, J, h): 							#Computes value of the Hamiltonian for Ising Model
    return -((J*matrix[x,y]*(tri_neighbours(matrix,x,y,N)))+h*matrix[x,y])


def tri_H_sq(matrix, x,y,N,  J, h): 							#computes and squares value of the Hamiltonian for Ising Model
    return (-((J*matrix[x,y]*(tri_neighbours(matrix,x,y,N)))+h*matrix[x,y]))**2

##########################################
"""Triangular Lattice Energy Functions"""
##########################################
def tri_energy(matrix,N, J, h): 							#returns average energy per spin 
    energy=0 										#initialise energy
    for y in range(0, N):
        for x in range(0,N):
	    Ham = tri_H(matrix, x,y, N, J, h)
            energy=energy+0.5*Ham 							#increments energy by 1/2 Hamiltonian
    return energy/N**2 									#averages energy per spin


def tri_energy_sq(matrix,N, J, h): 							#returns average squared energy per spin
    energy_sq=0
    for y in range(0, N):
        for x in range(0,N):
	    Ham_sq = tri_H_sq(matrix, x,y, N, J, h) 
            energy_sq=energy_sq+0.25*Ham_sq						#increments energy by 1/4 Hamiltonian_sq
    return energy_sq/N**2 								#averages energy per spin

##########################################
"""Magnetisation Functions"""
##########################################
def magnetisation(matrix,N):
	return (np.abs(np.sum(matrix)))/float(N**2.0) 					#sums up all values in the matrix(1 or -1), takes the absolute value and averages it

def magnetisation_sq(matrix,N):
	return (np.sum(matrix**2))/float(N**2.0) 					#matrix**2 squares all values in the matrix, then sums up all values in the matrix(1 or -1), takes the absolute value and averages it. 








