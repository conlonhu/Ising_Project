#!/usr/bin/env python
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import random

##########################################
"""1D Ising Model Modules"""
##########################################

#function that sums up all neighbouring sites of the inital position. %N imposes a boundary condition 
def neighbours(matrix,x,N): 
	s1 = matrix[(x+1) %N]
	s2 = matrix[(x-1) %N]
	return s1 + s2

#function for change in energy
def deltaE(matrix,x,N, J, h): 
	return -(2*J*matrix[x]*(neighbours(matrix,x,N)))+2*h*matrix[x] 

#metropolis algorithim 
def metropolis(matrix, counts,N, T, J, h, k):
	for p in range (counts):
		for x in range(0,N):
			dE = deltaE(matrix, x,N, J, h)
			if dE>=0: 
				matrix[x] *= -1 				#if energy change is greater than/equal to 0, flips spin
		    	else:
		    		r = random.random() 				#generates random number
		    		if r<np.exp(dE/(k*T)):		
		    				matrix[x] *= -1 		#if random number generated between 0 and 1 is less than exp^dE/k*T flips spin
	return matrix


def H(matrix, x, N, J, h): 							#Computes value of the Hamiltonian for Ising Model
    return -((J*matrix[x]*(neighbours(matrix,x,N)))+h*matrix[x])


def H_sq(matrix, x,N,  J, h): 							#computes and squares value of the Hamiltonian for Ising Model
    return (-((J*matrix[x]*(neighbours(matrix,x,N)))+h*matrix[x]))**2

##########################################
"""Magnetisation Functions"""
##########################################

def magnetisation(matrix,N):
	return (np.abs(np.sum(matrix)))/float(N) 				#sums up all values in the matrix(1 or -1), takes the absolute value and averages it

def magnetisation_sq(matrix,N):
	return (np.sum(matrix**2))/float(N) 					#matrix**2 squares all values in the matrix, then sums up all values in the matrix(1 or -1), takes the absolute value and averages it. 

##########################################
"""Energy Functions"""
##########################################

def energy(matrix,N, J, h): 							#returns average energy per spin 
	energy=0 								#initialise energy
	for x in range(0,N):
		Ham = H(matrix, x,N, J, h) 	
		energy=energy+0.5*Ham						#increments energy by 1/2 Hamiltonian
    	return energy/N 							#averages energy per spin


def energy_sq(matrix,N, J, h): 							#returns average squared energy per spin
	energy_sq=0
	for x in range(0,N):
		Ham_sq = H(matrix, x,N, J, h)
		energy_sq=energy_sq+0.25*Ham_sq			 		#increments energy by 1/4 Hamiltonian_sq
    	return energy_sq/N 							#averages energy per spin



