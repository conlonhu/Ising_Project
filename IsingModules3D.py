#!/usr/bin/env python
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import random

##########################################
"""3D Ising Model Modules"""
##########################################
#function that sums up all neighbouring sites of the inital position. %N imposes a boundary condition  
def neighbours(matrix,x,y,z,N): 
	s1 = matrix[x, (y+1) %N,z]
	s2 = matrix[x, (y-1) %N,z]
	s3 = matrix[(x+1) %N, y,z]
	s4 = matrix[(x-1) %N, y,z]
	s5 = matrix[x, y,(z+1)%N]
	s6 = matrix[x, y,(z-1)%N]
	return s1 + s2 + s3 + s4 + s5 + s6

#function for change in energy
def deltaE(matrix,x,y,z,N, J, h): 
	return -(2*J*matrix[x,y,z]*(neighbours(matrix,x,y,z,N)))+2*h*matrix[x,y,z] 

#metropolis algorithim 
def metropolis(matrix, counts,N, T, J, h, k):
	for p in range (counts):
		for z in range (0,N):
			for y in range(0, N):
			    for x in range(0,N):					#iterates [x,y,z] sequentially
				dE = deltaE(matrix,x,y,z,N,J,h)
			    	if dE>=0: 
			    		matrix[x,y,z] *= -1 				#if energy change is greater than/equal to 0, flips spin
			    	else:
			    		r = random.random() 				#generates random number
			    		if r<np.exp(dE/(k*T)):		
			    				matrix[x,y,z] *= -1 		#if random number generated between 0 and 1 is less than exp^dE/k*T flips spin
	return matrix


def H(matrix, x,y,z, N, J, h): 								#Computes value of the Hamiltonian for Ising Model
    return -((J*matrix[x,y,z]*(neighbours(matrix,x,y,z,N)))+h*matrix[x,y,z])

	
def H_sq(matrix, x,y,z, N,  J, h): 							#computes and squares value of the Hamiltonian for Ising Model
    return (-((J*matrix[x,y,z]*(neighbours(matrix,x,y,z,N)))+h*matrix[x,y,z]))**2


##########################################
"""Magnetisation Functions"""
##########################################

def magnetisation(matrix,N):
	return (np.abs(np.sum(matrix)))/float(N**3) 					#sums up all values in the matrix(1 or -1), takes the absolute value and averages it

def magnetisation_sq(matrix,N):
	return (np.sum(matrix**2))/float(N**3) 						#matrix**2 squares all values in the matrix, then sums up all values in the matrix(1 or -1), takes the absolute value and averages it. 

##########################################
"""Energy Functions"""
##########################################
def energy(matrix,N, J, h): 								#returns average energy per spin 
    	energy=0 									#initialise energy
	for z in range(0,N):
	    for y in range(0, N):
		for x in range(0,N):
		    Ham = H(matrix,x,y,z,N, J, h)
		    energy=energy+0.5*Ham						#increments energy by 1/2 Hamiltonian
	return energy/N**3 								#averages energy per spin


def energy_sq(matrix,N, J, h): 								#returns average squared energy per spin
    	energy_sq=0
	for z in range(0,N):
	    for y in range(0, N):
		for x in range(0,N):
	 	   Ham_sq = H_sq(matrix,x,y,z,N, J, h) 
		   energy_sq=energy_sq+0.25*Ham_sq					#increments energy by 1/4 Hamiltonian_sq
	return energy_sq/N**3 								#averages energy per spin


