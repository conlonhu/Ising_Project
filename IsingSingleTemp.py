#!/usr/bin/env python
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import random
import metropolis as met


#constants
size = 100 #dimensions of matrix
J = 1 
h = 0 #magnetic field
counts = 100
T = 1 #temperature, keep below ~2.26 for ferromagnetism, above 2.26 for paramagnetism
k=1 #boltzmann constant

class Ising_lattice: 
	def __init__(self,size):	#create initial matrix
		self.size=size
		self.matrix_lattice()
	
	def matrix_lattice(self):
		self.lattice = np.zeros((size,size), dtype=int) #creates initial matrix of zeroes
		for x in range(size):	
			for y in range(size):
				self.lattice[x,y]=random.choice([1,-1]) #randomly chooses values of 1 and -1 and appends matrix
    
                
lattice1=Ising_lattice(size)

print met.Ising(lattice1.lattice, counts,size, T, J, h, k)
plt.imshow(met.Ising(lattice1.lattice, counts,size, T, J, h, k),cmap='bwr',interpolation="none") 
plt.show() #plots Ising model in equilibrium 



