#!/usr/bin/env python
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import random


def deltaE(matrix, x, y, size, J, h):
	return -(2*J*matrix[x,y]*(matrix[x, (y+1) %size]+matrix[x, (y-1) %size] + matrix[(x+1) %size, y]  + matrix[(x-1) %size, y]))+2*h*matrix[x,y] #function for change in energy

#metropolis algorithim 
def Ising(matrix, counts,size, T, J, h, k):
	n = 0.0
	for n in range (counts):
		n += 1
		for y in range(0, size):
		    for x in range(0,size):
		    	if deltaE(matrix, x, y, size, J, h)>0: 
		    		matrix[x,y] *= -1 #if energy change is less than 0, flips spin
		    	else:
		    		r = random.random()
		    		if r<np.exp(deltaE(matrix, x, y, size, J, h)/(k*T)):		
		    				matrix[x,y] *= -1 #if random number generated between 0 and 1 is less than exp^dE/k*T flips spin
	return matrix



	
